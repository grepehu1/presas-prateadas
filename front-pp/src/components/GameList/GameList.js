import { getGameList } from '../../firebase/functions'
import useFetch from '../../hooks/useFetch'
import './GameList.css'
import Markdown from 'markdown-to-jsx';
import Modal from '../Modal/Modal';

export default function GameList() {
    const [data, loading, error] = useFetch(getGameList)

    if (loading){
        return <Modal message={'Loading...'}/>
    }
    if (error){
        return <Modal message={JSON.stringify(error)}/>
    }

    return (
      <div className='container game-list-wrapper'>
        {data && data.map(game => {
            const options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            };

            const description = String(game?.description).replace(/\_b/g,'\n')
            const seconds = game?.date?.seconds
            const dateObj = seconds ? new Date(seconds*1000) : null
            const dateStr = dateObj ? dateObj.toLocaleString('pt-BR', options) : ''

            if (!seconds){
                return (<></>)
            }

            return (
                <div key={seconds} className='row game-list-item'>
                    <div className='game-list-date'>
                        <p>{dateStr.toLocaleUpperCase()}</p>  
                    </div>
                    <div className='game-list-content'>
                        <Markdown>{description}</Markdown>
                    </div>
                </div>
            )
        })}
      </div>
    );
}
