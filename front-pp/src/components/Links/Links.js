import { getLinks } from '../../firebase/functions'
import useFetch from '../../hooks/useFetch'
import './Links.css'
import Markdown from 'markdown-to-jsx';
import Modal from '../Modal/Modal';

export default function Links() {
    const [data, loading, error] = useFetch(getLinks)

    if (loading){
        return <Modal message={'Loading...'}/>
    }
    if (error){
        return <Modal message={JSON.stringify(error)}/>
    }

    function renderLinks(){
        const body = String(data.body).replace(/\_b/g,'\n')

        return <Markdown>{body}</Markdown>
    }

    return (
      <div className='container'>
        {data && renderLinks()}
      </div>
    );
}
