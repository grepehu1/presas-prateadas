import './Modal.css'

export default function Modal({message}) {
    return (
        <div className="modal" tabindex="1">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-body">
                        <p>{message}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
