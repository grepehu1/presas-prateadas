import { getAbout } from '../../firebase/functions'
import useFetch from '../../hooks/useFetch'
import './About.css'
import Markdown from 'markdown-to-jsx';
import Modal from '../Modal/Modal';

export default function About() {
    const [data, loading, error] = useFetch(getAbout)

    if (loading){
        return <Modal message={'Loading...'}/>
    }
    if (error){
        return <Modal message={JSON.stringify(error)}/>
    }

    function renderAbout(){
        const body = String(data.body).replace(/\_b/g,'\n')

        return <Markdown>{body}</Markdown>
    }

    return (
      <div className='container'>
        {data && renderAbout()}
      </div>
    );
}
