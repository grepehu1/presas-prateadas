import './Header.css'

export default function Header({setPage}) {
  function handleClick(e, page){
    const navLinks = document.querySelectorAll('.nav-link') || []
    navLinks.forEach(nav => {
      nav.classList.remove('active')
    })
    e.target.classList.add('active')

    setPage(page)
  }

    return (
<nav className="header fixed-top navbar navbar-expand-lg navbar-dark">
  <div className="container-fluid">
    <button onClick={(e) => handleClick(e,'home')} className="navbar-brand" href="#">Presas Prateadas</button>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
          <button onClick={(e) => handleClick(e,'about')} className="nav-link" href="#">Sobre</button>
        </li>
        <li className="nav-item">
          <button onClick={(e) => handleClick(e,'links')} className="nav-link" href="#">Links Importantes</button>
        </li>
        <li className="nav-item">
          <button onClick={(e) => handleClick(e,'game-list')} className="nav-link" href="#">Mesas Marcadas</button>
        </li>
      </ul>
    </div>
  </div>
</nav>
    )
}
