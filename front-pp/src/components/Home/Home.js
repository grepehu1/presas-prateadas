import './Home.css'
import dwImg from '../../assets/images/dungeon-world.png'

export default function Home() {
    return (
      <div className='container home-page'>
        <h1>Presas Prateadas</h1>
        <figure>
          <img src={dwImg}></img>
        </figure>
      </div>
    );
}
