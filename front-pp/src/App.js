import { useState } from 'react';
import './App.css';
import GameList from './components/GameList/GameList';
import Header from './components/Header/Header';
import About from './components/About/About';
import Links from './components/Links/Links';
import Home from './components/Home/Home';

function App() {
  const [page, setPage] = useState('home')

  return (
    <div className="container app-wrapper">
      <Header setPage={setPage}/>
      <div className='app-content'>
        {page === 'home' && <Home/>}
        {page === 'links' && <Links/>}
        {page === 'about' && <About/>}
        {page === 'game-list' && <GameList/>}
      </div>
    </div>
  );
}

export default App;
