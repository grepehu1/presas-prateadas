import { collection, doc, getDoc, getDocs } from 'firebase/firestore/lite';
import { db } from './init';

export async function getGameList() {
    const col = collection(db, 'game-list');
    const docs = await getDocs(col);
    const list = docs.docs.map(doc => doc.data());
    return list;
}

export async function getInfos() {
    const col = collection(db, 'infos');
    const docs = await getDocs(col);
    const list = docs.docs.map(doc => doc.data());
    return list;
}

export async function getInfo(id) {
    const docRef = doc(db, 'infos', id);
    const docSnap = await getDoc(docRef);
    const info = docSnap.data();
    return info;
}

export async function getAbout() {
    const about = await getInfo('about')
    return about;
}

export async function getLinks() {
    const about = await getInfo('links')
    return about;
}