// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from 'firebase/firestore/lite';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBXfCaUeiFtQ63W9xyaZX0nVzP5CmXev4I",
  authDomain: "presas-prateadas.firebaseapp.com",
  projectId: "presas-prateadas",
  storageBucket: "presas-prateadas.appspot.com",
  messagingSenderId: "661464499772",
  appId: "1:661464499772:web:68554057aeb31572a4e539",
  measurementId: "G-9DENCVMBYK"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const db = getFirestore(app);
