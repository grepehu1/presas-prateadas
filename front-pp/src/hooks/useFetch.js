import {useState, useEffect} from 'react'

export default function useFetch(callback) {
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)

    useEffect(() => {
        async function fetcher(){
            try {
                const newData = await callback()
                setData(newData)
                setLoading(false)
            } catch (e) {
                setError(e)
                setLoading(false)
            }
        }
        fetcher()
    }, [callback])

    return [data, loading, error]
}
