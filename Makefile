include environments/dev/.env

FCWD := /app
USER := $(shell /usr/bin/id -u)
FRONT := front-pp

up:## Build the app container image (if it doesn't exists) and runs the containers
	docker-compose -f environments/dev/docker-compose.yml up

upBuild:## Rebuild the app container image and runs the containers
	docker-compose -f environments/dev/docker-compose.yml up --build

down:## Stop and remove the containers that was created by 'make up' command
	docker-compose -f environments/dev/docker-compose.yml down

install:## Runs 'npm install'
	docker exec -it $(FRONT) sh -c "(cd $(FCWD) && npm install)"

build:## Runs 'npm run build'
	docker exec -it $(FRONT) sh -c "(cd $(FCWD) && npm run build)"

grant:## Grant permissions to all files (Use it if you have access permissions issues)
	bash -c "sudo chmod -R a+rw . && sudo chown -R $(USER):$(USER) ."

access:## Run an interactive bash session on front-end container
	docker exec -it $(FRONT) bash

deploy:## Run an interactive bash session on front-end container
	cd front-pp && npm run build && cd ..
	firebase deploy